package cat.jmir.fragments_advanced.fragments;

import android.app.Activity;
import android.app.Fragment;

public class OnButtonClickedOnFragment extends Fragment {
	OnButtonClickedOnFragmentListener mCallback;
	
	// La actividad contenedora debe implementar esta interfaz
    public interface OnButtonClickedOnFragmentListener {
        public void onButtonClicked(String missatge);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // Nos aseguramos de que la actividad contenedora haya implementado la
        // interfaz de retrollamada. Si no, lanzamos una excepción
        try {
            mCallback = (OnButtonClickedOnFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " ha d'implementar OnButtonClickedOnFragmentListener");
        }
    }
}