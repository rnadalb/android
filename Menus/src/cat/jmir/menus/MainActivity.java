package cat.jmir.menus;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		String missatge = "";
		
		switch (id) {
		case R.id.action_opcio1:
			missatge = getResources().getString(R.string.action_opcio1) + " escollida !!!";
			Toast.makeText(getApplicationContext(), missatge, Toast.LENGTH_SHORT).show();
			return true;
		case R.id.action_opcio2:
			missatge = getResources().getString(R.string.action_opcio2) + " escollida !!!";
			Toast.makeText(getApplicationContext(), missatge, Toast.LENGTH_SHORT).show();
			return true;
		case R.id.action_opcio3:
			missatge = getResources().getString(R.string.action_opcio3) + " escollida !!!";
			Toast.makeText(getApplicationContext(), missatge, Toast.LENGTH_SHORT).show();
			return true;
		case R.id.action_opcio4:
			missatge = getResources().getString(R.string.action_opcio4) + " escollida !!!";
			Toast.makeText(getApplicationContext(), missatge, Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

		
	}
}
