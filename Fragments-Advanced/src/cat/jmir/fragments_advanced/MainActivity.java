package cat.jmir.fragments_advanced;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import cat.jmir.fragments_advanced.fragments.OnButtonClickedOnFragment.OnButtonClickedOnFragmentListener;
import cat.jmir.infovistes.R;

public class MainActivity extends Activity implements OnButtonClickedOnFragmentListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			
		}
	}
	
	@Override
	public void onButtonClicked(String missatge) {
		Log.i("onButtonClicked()", missatge);
		
		if (this.findViewById(R.id.txtNomFragment1) != null)
		{
			Log.i("MainActivity", "onButtonClick()");
			TextView txt = (TextView) this.findViewById(R.id.txtMissatgeRebut);
			txt.setText(missatge);
		}
	}
}
