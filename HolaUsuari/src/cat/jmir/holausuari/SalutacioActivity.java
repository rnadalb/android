package cat.jmir.holausuari;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class SalutacioActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_salutacio);
		
		//Obtenim una referència als controls de la interfície
        TextView txtSalutacio = (TextView)findViewById(R.id.txtSalutacio);
        
        //Recuperem la informació empaquetada (del bundle !!!)
        Bundle bundle = this.getIntent().getExtras();
        
        // Obtenim el missatge de salutacio des de els resources
        String iniciMissatge = getResources().getString(R.string.msg_hola);
        
        //Mostrem el missatge
        txtSalutacio.setText(iniciMissatge + " " + bundle.getString("NOM") + "!!!");
	}
}
