package cat.jmir.holausuarifragments;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (savedInstanceState == null) {
			// Creem una instància del fragment a mostrar
			Fragment_Principal fragment = new Fragment_Principal();
			
			// Creem una transacció
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			
			// Canviem el contenidor (FramLayout) pel fragment desitjat 
			transaction.add(R.id.fragment_container, fragment);
			
			// Confirmem el canvi
			transaction.commit(); 
		}
	}
}