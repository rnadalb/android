package cat.jmir.guardaestat;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private static final String STATE_NOM = "NOM";
	private static final String STATE_COGNOMS = "COGNOM";
	private static final String STATE_DNI = "DNI";
	
	private EditText mNomText;
	private EditText mCognomsText;
	private EditText mDniText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mNomText = (EditText) findViewById(R.id.edtNomUsuari);
		mCognomsText = (EditText) findViewById(R.id.edtCognomsUsuari);
		mDniText = (EditText) findViewById(R.id.edtDniUsuari);
		
		Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_SHORT).show();
	}

	// Guarda l'estat de l'activitat al rotar la pantalla o al canviar de context (entra una trucada)
	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {		
		savedInstanceState.putString(STATE_NOM, mNomText.getText().toString());
		savedInstanceState.putString(STATE_COGNOMS, mCognomsText.getText().toString());
		savedInstanceState.putString(STATE_DNI, mDniText.getText().toString());
		Toast.makeText(getApplicationContext(), "onSaveInstanceState()", Toast.LENGTH_SHORT).show();
		
		// Sempre al final !!!
		super.onSaveInstanceState(savedInstanceState);
	}
	
	// Restaura l'estat de l'activitat al rotar la pantalla o al canviar de context (entra una trucada)
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		// Si ho fem aqui enlloc de fer-ho al onCreate() no cal
		// comprovar si el bundle es buit	
		String nom = savedInstanceState.getString(STATE_NOM);
		Log.i("nom", nom);
				
		mNomText.setText(savedInstanceState.getString(STATE_NOM));
		mCognomsText.setText(savedInstanceState.getString(STATE_COGNOMS));
		mDniText.setText(savedInstanceState.getString(STATE_DNI));
		Toast.makeText(getApplicationContext(), "onRestoreInstanceState()", Toast.LENGTH_SHORT).show();
	}	
}
