package cat.jmir.holausuarifragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Fragment_Salutacio extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_salutacio, container, false);
		
		Bundle bundle = this.getArguments();
		String nom = bundle.getString("NOM", "No has escrit un nom, BANDARRA!!!");
		TextView txtSalutacio = (TextView) view.findViewById(R.id.txtSalutacio);
		String msgHola = getResources().getString(R.string.msg_hola);
		txtSalutacio.setText(msgHola + " " + nom + "!!!");
		return view;
	}
}