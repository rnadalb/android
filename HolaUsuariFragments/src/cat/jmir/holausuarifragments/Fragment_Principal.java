package cat.jmir.holausuarifragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_Principal extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		final View view = inflater.inflate(R.layout.fragment_principal, container, false);
		
		if (view != null) {
			Button boto = (Button) view.findViewById(R.id.btnHola);
			boto.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.i("Principal.onClick()", "Clicked");
					
					TextView txtNomUsuari = (TextView)view.findViewById(R.id.txtNomUsuari);
					if (txtNomUsuari.getText().length() > 0) {
						Fragment_Salutacio fragment = new Fragment_Salutacio();
						FragmentTransaction transaction = getFragmentManager().beginTransaction();
						Bundle bundle = new Bundle();
						bundle.putString("NOM",txtNomUsuari.getText().toString());
						fragment.setArguments(bundle);
						transaction.replace(R.id.fragment_container, fragment);
						transaction.addToBackStack(null);
						transaction.commit();
					}
					else {
						Toast.makeText(view.getContext(), "Escriu el teu nom", Toast.LENGTH_LONG).show();
					}
				}
			});
		}
		return view;
	}
}