package cat.jmir.activitatambresultat;

// Com és habitual declarem una interfície per a declarar totes les constants
// Alerta !! No és recomanable utilitzar Constants com a nom, perquè ja existeix
//           No passa res, però així no et confondràs
public interface Constantes {
	int OBRE_SEGONA_REQUEST = 1;
}
