/**
 *  startActivityForResult()
 * 
 *  En aquest exemple podràs provar com passar informació resultant
 *  des d'una activitat a un altra.
 *  
 * 
 *  http://developer.android.com/training/basics/intents/result.html
 */
package cat.jmir.activitatambresultat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Button btnSegona;
	private TextView txtResultat;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Referències als controls que ens calen
		btnSegona = (Button) findViewById(R.id.btnCridaActivitat2);
		txtResultat = (TextView) findViewById(R.id.txtResultat);
		
		// Gestiones l'event onClick
		btnSegona.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, SegonaActivity.class);
				// Llacem la segona activitat però esperarem un resultat
				startActivityForResult(intent, Constantes.OBRE_SEGONA_REQUEST);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		// Comprovem que és el codi que volem
		// Si en tens molts, prova amb un switch ;-)
		if (requestCode == Constantes.OBRE_SEGONA_REQUEST) {
				String text = data.getStringExtra("TEXT");
				if (text.length() > 0)
					txtResultat.setText("Des de l'activitat 2 m'han passat: " + text);
				else
					Toast.makeText(getApplicationContext(), "Me l'has enviat en blanc. Mala persona !!!", Toast.LENGTH_SHORT).show();
		}
	}	
}