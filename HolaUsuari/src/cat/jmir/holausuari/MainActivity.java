package cat.jmir.holausuari;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Obtenim una referència als controls de l'interfície
		final EditText txtNomUsuari = (EditText) findViewById(R.id.txtNomUsuari);
		final Button btnHola = (Button) findViewById(R.id.btnHola);

		// Implementem l'event onClick del botó
		btnHola.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Creem un Intent --> Intent(Context, Activity a assignar)
				Intent intent = new Intent(MainActivity.this, SalutacioActivity.class);

				// Empaquetem (bundle) les dades a passar a l'Activity 
				Bundle b = new Bundle();
				b.putString("NOM", txtNomUsuari.getText().toString());

				// Afegim les dades a l'Intent
				intent.putExtras(b);

				// Iniciem l'Activity
				startActivity(intent);
			}
		});
	}
}