package cat.jmir.fragments_advanced.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cat.jmir.infovistes.R;

public class Fragment_2 extends OnButtonClickedOnFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.fragment_2, container, false); 
		if (fragmentView != null)
		{
			Button btnEnviaMissatge = (Button) fragmentView.findViewById(R.id.btnEnviaMissatge);
			
			btnEnviaMissatge.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					EditText edtMissatge = (EditText)fragmentView.findViewById(R.id.txtMissatge);
					if (edtMissatge.getText().length() >0)
						mCallback.onButtonClicked(edtMissatge.getText().toString());
					else
					{
						Toast toast = Toast.makeText(fragmentView.getContext(), "Escriu alguna cosa ;-)", Toast.LENGTH_LONG);
						toast.show();
					}
				}
			});	
		}
		return fragmentView;
	}
}
