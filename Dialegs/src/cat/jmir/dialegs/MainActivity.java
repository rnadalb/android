package cat.jmir.dialegs;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	ArrayList<Integer> mElementsSeleccionats;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Obtenim les referències als controls
		Button btnDialegSimple = (Button) findViewById(R.id.btnAlertDialog);
		Button btnLlistaSimple = (Button) findViewById(R.id.btnDialegLlista);
		Button btnSingleChoice = (Button) findViewById(R.id.btnSingleChoice);
		Button btnSingleChoiceBoto = (Button) findViewById(R.id.btnSingleChoiceBoto);
		Button btnMultipleChoice = (Button) findViewById(R.id.btnMultipleChoice);
		
		// Generem els events onClick
		btnDialegSimple.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				cuadreDialegSimple();
			}
		});
		
		btnLlistaSimple.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				cuadreDialegLlistaSimple();
			}
		});
		
		btnSingleChoice.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				cuadreDialegSingleChoiceSenseBoto();
			}
		});
		
		btnSingleChoiceBoto.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				cuadreDialegSingleChoiceAmbBoto();
			}
		});
		
		btnMultipleChoice.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				cuadreDialegSeleccioMultiple();
			}
		});
	}
	
	private void cuadreDialegSimple() {
		// Creem una instància per al nostre generador de cuadres de dialeg
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// Indiquem el títol del cuadre de dialeg
		builder.setTitle(R.string.dialog_title);
		// Indiquem el missatge o pregunta del cuadre de dialeg
		builder.setMessage(R.string.dialog_message);
		// Indiquem una icona
		builder.setIcon(R.drawable.ic_launcher);
		
		// Boto que reacciona com a positiu 
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	               // L'usuari ha pres OK
	        	   Toast.makeText(getApplicationContext(), R.string.ok_message, Toast.LENGTH_SHORT).show();
	           }
	       });
		
		// Boto que reacciona com a negatiu
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	   // L'usuari ha pres cancelar --> Ha cancelat l'acció
	        	   Toast.makeText(getApplicationContext(), R.string.cancel_message, Toast.LENGTH_SHORT).show();
	           }
	       });
		// Si no cridem a show no ens mostrarà res
		builder.show();
	}
	
	private void cuadreDialegLlistaSimple() {
		// Creem una instància per al nostre generador de cuadres de dialeg
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_list_title);
        builder.setItems(R.array.llista_elements, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	// L'argument 'which' conté l'index (0, n-1) de la posició de l'element seleccionat
            	String[] mArrayItems = getResources().getStringArray(R.array.llista_elements);
            	Toast.makeText(getApplicationContext(),"Has triat l'opció: " + mArrayItems[which], Toast.LENGTH_SHORT).show();
            	// També es pot utilitzar un switch !!!
            }
        });
        builder.show();
	}
	
	private void cuadreDialegSingleChoiceSenseBoto() {
		// Creem una instància per al nostre generador de cuadres de dialeg
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_list_title);
		builder.setSingleChoiceItems(R.array.llista_elements, 0, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Tanquem el cuadre  de dialeg
				dialog.dismiss();
				// L'argument 'which' conté l'index (0, n-1) de la posició de l'element seleccionat
            	String[] mArrayItems = getResources().getStringArray(R.array.llista_elements);
            	Toast.makeText(getApplicationContext(),"Has triat l'opció: " + mArrayItems[which], Toast.LENGTH_SHORT).show();
            	// També es pot utilitzar un switch !!!
			}
		});
		builder.show();
	}
	
	private void cuadreDialegSingleChoiceAmbBoto() {
		// Creem una instància per al nostre generador de cuadres de dialeg
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_list_title);
		builder.setSingleChoiceItems(R.array.llista_elements, 0, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Aqui no fem res, ja que volem que l'usuari premi el botó
				// Es podria passar una variable global al mètode per identificar
				// l'opció escollida i evitar el codi "extrany" que obté la posició
				// seleccionada (mira avall --> int posicioSeleccionada
			}
		});
		
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// tanquem el cuadre de dialeg
				dialog.dismiss();
				// L'argument 'which' conté l'index (0, n-1) de la posició de l'element seleccionat
				//            Al afegir el botó l'última posició és el propi boto i per tant dona un error
				//            en temps d'execució. Per aquest motiu cal fer aquest truquet
				int posicioSeleccionada = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
				// Obtenim l'array dels recursos
				String[] arrayRecursos = getResources().getStringArray(R.array.llista_elements);
            	Toast.makeText(getApplicationContext(),"Has triat l'opció: " + arrayRecursos[posicioSeleccionada], Toast.LENGTH_SHORT).show();
            	// També es pot utilitzar un switch !!!
			}
		});
		builder.show();
	}
	
	private void cuadreDialegSeleccioMultiple() {
		mElementsSeleccionats = new ArrayList<Integer>();
		// Creem una instància per al nostre generador de cuadres de dialeg
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_list_title);
		// Esborrem les dades de l'array
		mElementsSeleccionats.clear();
		builder.setMultiChoiceItems(R.array.llista_elements, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    // Si l'usuari marca l'element, l'afegim a l'array
                    mElementsSeleccionats.add(which);
                } else if (mElementsSeleccionats.contains(which)) {
                    // En cas contrati si l'element ja està a l'array, l'eliminem 
                    mElementsSeleccionats.remove(Integer.valueOf(which));
                }
            }
        });
		// Boto acceptar
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Obtenim l'array dels recursos
				String[] arrayRecursos = getResources().getStringArray(R.array.llista_elements);
				String missatge = "";
				// Si l'usuari no ha seleccionat res
				if (!mElementsSeleccionats.isEmpty())
				{
					// Si entra és perquè hi han dades ...
					// Recorrem l'array amb els elements seleccinats i mostrem un missatge
					for (int i = 0; i < mElementsSeleccionats.size(); i++) {
						// Recorda que dins de l'array trobem la posició que ha triat l'usuari
						int valor = mElementsSeleccionats.get(i);
						missatge = missatge + ", " + arrayRecursos[valor];
					}
					Toast.makeText(getApplicationContext(),"Has triat les opcions: " + missatge, Toast.LENGTH_SHORT).show();
				}
				else
					Toast.makeText(getApplicationContext(),"No has triat res", Toast.LENGTH_SHORT).show();
			}
		});
		
		// Boto que reacciona com a negatiu
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	   // L'usuari ha pres cancelar --> Ha cancelat l'acció
	        	   Toast.makeText(getApplicationContext(), R.string.cancel_message, Toast.LENGTH_SHORT).show();
	           }
	       });
		builder.show();
	}
}
