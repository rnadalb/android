package cat.jmir.activitatambresultat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SegonaActivity extends Activity {

	Button btnTanca;
	EditText edtMissatge;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_segona);
		
		//Obtenim referències alb controls que necessitem
		edtMissatge = (EditText)findViewById(R.id.edtTextSegonaActivitat);
		btnTanca = (Button) findViewById(R.id.btnTancaSegona);
		
		// Event onCLick
		btnTanca.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				generaInformacio();
				// finalitzem l'activitat
				finish();
			}
		});	
	}
	
	// Reescrivim el comportament del botó enrere
	// En aquest cas guardem la informació a passar
	// Prova comentar-ho i comprovaràs que si estàs a la segona
	// activitat, l'app fallarà, perquè espera un resultat
	@Override
	public void onBackPressed() {
		generaInformacio();
		super.onBackPressed();
	}
	
	private void generaInformacio() {
		// Guardem en un obnjecte Intent tot allò que volem passar
		Intent intent = new Intent();
		// putExtra(CLAU, VALOR) --> La clau ha de ser exactament la mateixa
		//                           En cas contrari falla
		intent.putExtra("TEXT",edtMissatge.getText().toString());
		// El resultat de l'activitat es ...
		setResult(Constantes.OBRE_SEGONA_REQUEST, intent);
	}
}